import 'pixi'
import 'p2'
import * as Phaser from 'phaser-ce'

import { Car } from "./objects/car";
import { CONSTANTS } from './constants';
import { TrackGenerator, TRACK_GENERATOR_MODE } from './generators/trackGenerator';
import { Track } from './objects/track';
import { GameTimer } from './objects/timer';

declare global {
  interface Window { Phaser: any; }
}
window.Phaser = Phaser || {};

const sprites: any = {
  car: '/assets/racing-pack/PNG/Cars/car_black_small_1.png',
  grass: '/assets/racing-pack/PNG/Tiles/Grass/land_grass11.png',
  trackHorizontal: '/assets/racing-pack/PNG/Tiles/Asphalt road/road_asphalt02.png',
  trackHorizontalStart: '/assets/racing-pack/PNG/Tiles/Asphalt road/road_asphalt43.png',
  trackVertical: '/assets/racing-pack/PNG/Tiles/Asphalt road/road_asphalt01.png',
  trackNEAngle: '/assets/racing-pack/PNG/Tiles/Asphalt road/road_asphalt03.png',
  trackNWAngle: '/assets/racing-pack/PNG/Tiles/Asphalt road/road_asphalt05.png',
  trackSWAngle: '/assets/racing-pack/PNG/Tiles/Asphalt road/road_asphalt41.png',
  trackSEAngle: '/assets/racing-pack/PNG/Tiles/Asphalt road/road_asphalt39.png',
}

export default class Game {
  private game: Phaser.Game;
  private config: any = {
    type: Phaser.AUTO,
    width: window.innerWidth,
    height: window.innerHeight,
    state: {
      create: this.create.bind(this),
      preload: this.preload.bind(this),
      update: this.update.bind(this)
    },
    backgroundColor: "#EEE"
  }
  private car: Car;
  private track: Track;
  private gameTimer: GameTimer;

  private cursors: any;

  private isFirstStart: boolean = true;
  
  private isOnFinishSprite: boolean = false;

  constructor() {
    this.game = new Phaser.Game(this.config);
  }
  preload() {
    this.game.load.image('Car', sprites.car);

    this.game.load.image('Grass', sprites.grass);

    this.game.load.image('TrackHorizontal', sprites.trackHorizontal);
    this.game.load.image('TrackHorizontalStart', sprites.trackHorizontalStart);
    this.game.load.image('TrackVertical', sprites.trackVertical);
    this.game.load.image('TrackNEAngle', sprites.trackNEAngle);
    this.game.load.image('TrackNWAngle', sprites.trackNWAngle);
    this.game.load.image('TrackSWAngle', sprites.trackSWAngle);
    this.game.load.image('TrackSEAngle', sprites.trackSEAngle);
  }
  create() {
    this.game.world.setBounds(0, 0, CONSTANTS.WORD_WIDTH, CONSTANTS.WORLD_HEIGHT);
    this.game.add.tileSprite(0, 0, CONSTANTS.WORD_WIDTH, CONSTANTS.WORLD_HEIGHT, 'Grass');

    this.game.physics.startSystem(Phaser.Physics.P2JS);

    let trackGenerator = new TrackGenerator(this.game, TRACK_GENERATOR_MODE.SIMPLE);
    this.track = trackGenerator.generate(CONSTANTS.TRACK_X_TILES_COUNT, CONSTANTS.TRACK_Y_TILES_COUNT);

    this.car = new Car(this.game, 'Car');

    this.gameTimer = new GameTimer(this.game);
    

    this.cursors = this.game.input.keyboard.createCursorKeys();
    this.game.camera.follow(this.car.sprite);
  }

  update() {
		if (this.cursors.left.isDown) {
			this.car.turn('left');
		}
		if (this.cursors.right.isDown) {
			this.car.turn('right');
		}
		if (this.cursors.down.isDown) {
			this.car.brake();
		}
		if (this.cursors.up.isDown) {
			this.car.accelerate();
    }
    if (!this.cursors.up.isDown && !this.cursors.down.isDown) {
      this.car.decelerate();
    }
    if (!this.cursors.left.isDown && !this.cursors.right.isDown) {
      this.car.align();
    }

    this.track.updateCheckpoint(this.car);
    this.checkStartOverlap();
  }

  checkStartOverlap() {
    if (this.track.isOnStart(this.car)) {
      if (!this.isOnFinishSprite && (this.track.isTrackFinished()) || this.isFirstStart) {
        this.gameTimer.addToTopTime();
        this.isOnFinishSprite = true;
        this.gameTimer.start();

        this.track.resetCheckpoints();
        this.isFirstStart = false;
      } 
    }else {
      this.isOnFinishSprite = false;
    }
  }
}