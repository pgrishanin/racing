interface IGameObject {
  sprite: Phaser.Sprite;
}

interface SpriteAlias {
  url: string;
  alias: string;
}