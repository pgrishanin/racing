import { Track } from "../objects/track";

export interface IGraphNode {
  coordinate: ICoordinate
  prevNode?: IGraphNode;
  nextNode?: IGraphNode;
  visited: boolean;
  checkpoint?: boolean;
  isStart: boolean;
  sprite?: Phaser.Sprite;
}

export interface ICoordinate {
  x: number;
  y: number;
}

export const TRACK_GENERATOR_MODE = {
  SIMPLE: 'simple',
  RANDOM: 'random'
}

export class TrackGenerator {
  private matrix: IGraphNode[][] = [];
  public startNode: IGraphNode;
  public path: IGraphNode[] = [];

  constructor(private game: Phaser.Game, private mode: string) {

  }

  public generate(width: number, height: number) {
    this.generateStartMatrix(width, height);

    switch (this.mode) {
      case TRACK_GENERATOR_MODE.RANDOM:
        // TODO: implement procedural path generator
        // this.generateRandomPath();
        break;
      case TRACK_GENERATOR_MODE.SIMPLE:
      default:
        this.generateSimplePath();
        break;
    }

    return new Track(this.game, this.matrix);
  }

  private generateStartMatrix(width: number, height: number) {
    for (let i=0; i < width; i++) {
      this.matrix[i] = [];
      for (let j=0; j < height; j++) {
        this.matrix[i][j] = {
          coordinate: {
            x: i,
            y: j
          },
          visited: false,
          isStart: false
        }
      }
    }
  }


  private generateSimplePath() {

    this.startNode = this.matrix[4][0];
    this.startNode.isStart = true;
    this.startNode.visited = true;

    let currentNode: IGraphNode = this.startNode,
        nextNode: IGraphNode;

    while (nextNode != this.startNode) {
      if ((currentNode.coordinate.x + 1) < this.matrix.length && !currentNode.coordinate.y) { // Top line, horizontal move
        nextNode = this.matrix[currentNode.coordinate.x + 1][currentNode.coordinate.y];
      } else if ((currentNode.coordinate.x + 1) == this.matrix.length && (currentNode.coordinate.y + 1) < this.matrix[0].length) { // Right line, vertical move
        nextNode = this.matrix[currentNode.coordinate.x][currentNode.coordinate.y + 1];
      } else if (currentNode.coordinate.x && (currentNode.coordinate.y + 1) == this.matrix[0].length) { // Bottom line horizontal move
        nextNode = this.matrix[currentNode.coordinate.x - 1][currentNode.coordinate.y];
      } else if (!currentNode.coordinate.x && (currentNode.coordinate.y - 1) >= 0) {
        nextNode = this.matrix[currentNode.coordinate.x][currentNode.coordinate.y - 1];
      }

      nextNode.prevNode = currentNode;
      currentNode.nextNode = nextNode;
      nextNode.visited = true;
      currentNode = nextNode;
    }
  }
}