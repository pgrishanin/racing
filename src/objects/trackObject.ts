import { GameObject } from "./gameObject";
import { IGraphNode } from "../generators/trackGenerator";

export class TrackObject extends GameObject {
  public checkpoint: boolean = false;
  public isStart: boolean = false;
  public node: IGraphNode;

  constructor(game: Phaser.Game, alias: string, graphNode: IGraphNode) {
    super(game, alias);

    this.node = graphNode;
  }
}