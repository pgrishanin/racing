export class GameObject implements IGameObject {
  public sprite: Phaser.Sprite;

  constructor(game: Phaser.Game, private alias: string) {
    this.sprite = game.add.sprite(50, 100, alias);
  }
}