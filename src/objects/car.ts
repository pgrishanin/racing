import { GameObject } from "./gameObject";

export class Car extends GameObject {

  private velocity: number = 0;
  private angle: number = 0;

  private maxVelocity: number = 500;
  private maxReverseVelocity: number = -100;
  private accelerationSpeed: number = 10;
  private brakeSpeed: number = 10;
  private decelerateSpeed: number = 5;

  constructor(game: Phaser.Game, alias: string) {
    super(game, alias);

    game.physics.p2.enable(this.sprite);
    this.sprite.body.angle = 90;
    this.sprite.body.x = 128;
    this.sprite.body.y = 64;
  }

  move() {
    this.sprite.body.velocity.x = this.velocity * Math.cos((this.sprite.angle-90)*0.01745);
    this.sprite.body.velocity.y = this.velocity * Math.sin((this.sprite.angle-90)*0.01745);
  }

  accelerate() {
    if (this.velocity <= this.maxVelocity) {
      this.velocity += this.accelerationSpeed;
    }
    this.move();
  }
  decelerate() {
    if (this.velocity >= this.decelerateSpeed) {
      this.velocity -= this.decelerateSpeed;
    } else if (this.velocity <= -this.decelerateSpeed) {
      this.velocity += this.decelerateSpeed;
    }
    this.move();
  }
  brake() {
    if (this.velocity >= this.maxReverseVelocity) {
      this.velocity -= this.brakeSpeed;
    }
    this.move();             
  }

  turn(direction: string) {
    switch (direction) {
      case 'left':
        this.sprite.body.angularVelocity = -5*(this.velocity/1000);
        break;
      case 'right':
        this.sprite.body.angularVelocity = 5*(this.velocity/1000);
        break;
    }
  }
  align() {
    this.sprite.body.angularVelocity = 0;
  }
}