export class GameTimer {

  private lapTimes: number[] = [];
  private topTimeText: Phaser.Text;
  private timer: Phaser.Timer;
  private timerText: Phaser.Text;
  private currentLapTime: number = 0;

  constructor(private game: Phaser.Game) {
    this.timer = this.game.time.create(false);
    this.timer.loop(100, this.updateTimerText, this);
    this.timerText = this.game.add.text(0, 0, "Cross the finish line to start", { font: "32px Arial", fill: "#ffffff", align: "center"});
    this.timerText.fixedToCamera = true;
    this.timerText.cameraOffset.setTo(<number>this.game.config.height / 2 - 50, <number>this.game.config.width / 4);
  }
  
  public start() {
    this.timer.start();
  }

  public addToTopTime() {
    if (this.currentLapTime) {
      this.lapTimes.push(this.currentLapTime);
    }

    // Draw best time table
    if (!this.topTimeText) {
      this.topTimeText = this.game.add.text(0, 0, "", { font: "24px Arial", fill: "#ffffff", align: "center"});
      this.topTimeText.fixedToCamera = true;
      this.topTimeText.cameraOffset.setTo(<number>this.game.config.height / 2 - 50, <number>this.game.config.width / 4 + 100);
    }
    let resultText = 'Best results: ';
    this.lapTimes
      .sort((a,b) => {
        return a-b;
      })
      .slice(0, 4)
      .forEach(curTime => {
        resultText += `\n ${this.formatTime(curTime)}`;
      });
    resultText += `\n Last lap: ${this.formatTime(this.currentLapTime)}`
    this.topTimeText.setText(resultText);
    
    // reset current lap time
    this.currentLapTime = 0;
  }

  private updateTimerText() {
    this.currentLapTime += 1;

    this.timerText.setText(`Lap time: ${this.formatTime(this.currentLapTime)}`);
  }

  private formatTime(time: number) {
    let min = Math.floor(time / 600),
        sec = Math.floor((time - min * 600) / 10),
        ms = time - min * 600 - sec * 10;
    return `${min < 10 ? '0' + min : min}:${sec < 10 ? '0' + sec : sec}:${ms}`;
  }
}