import { TrackObject } from './trackObject';
import { IGraphNode } from '../generators/trackGenerator';
import { GameObject } from './gameObject';


export const SPRITE_SIZE = {
  WIDTH: 128,
  HEIGHT: 128
}

export class Track {
  private path: TrackObject[] = [];
  private startNode: TrackObject;

  constructor(private game: Phaser.Game, matrix: IGraphNode[][]) {
    for (let i=0; i < matrix.length; i++) {
      for (let j=0; j < matrix[0].length; j++) {
        if (matrix[i][j].visited) {
          let spriteAlias = '';
          switch (true) {
            case matrix[i][j].nextNode.coordinate.x == i && matrix[i][j].prevNode.coordinate.y != j: // vertical node
              spriteAlias = 'TrackVertical';
              break;
            case matrix[i][j].nextNode.coordinate.y == j && matrix[i][j].prevNode.coordinate.x != i: // horizontal node
              if (matrix[i][j].isStart) {
                spriteAlias = 'TrackHorizontalStart';
              } else {
                spriteAlias = 'TrackHorizontal';
              }
              break;
            case matrix[i][j].nextNode.coordinate.x > i && matrix[i][j].prevNode.coordinate.y > j: // top-left angle
              spriteAlias = 'TrackNEAngle';
              break;
            case matrix[i][j].nextNode.coordinate.y > j && matrix[i][j].prevNode.coordinate.x < i: // top-right angle
              spriteAlias = 'TrackNWAngle';
              break;
            case matrix[i][j].nextNode.coordinate.x < i && matrix[i][j].prevNode.coordinate.y < j: // bottom-right angle
              spriteAlias = 'TrackSWAngle';
              break;
            case matrix[i][j].nextNode.coordinate.y < j && matrix[i][j].prevNode.coordinate.x > i: // bottom-left angle
              spriteAlias = 'TrackSEAngle';
              break;
          }

          let trackObject = new TrackObject(this.game, spriteAlias, matrix[i][j]);
          trackObject.sprite.x = SPRITE_SIZE.WIDTH * i;
          trackObject.sprite.y = SPRITE_SIZE.HEIGHT * j;

          // save start node
          if (matrix[i][j].isStart) {
            this.startNode = trackObject;
          }

          this.path.push(trackObject);
        }
      }
    }

    var blockShape = this.game.add.bitmapData(1024, 1024);
    blockShape.ctx.rect(0, 0, 1024, 1024);
    let centerWall = this.game.add.sprite(640, 640, blockShape);
    this.game.physics.p2.enable(centerWall);
    centerWall.body.static = true;
  }

  resetCheckpoints() {
    this.path.map(node => {
      node.checkpoint = false;
    })
  }

  updateCheckpoint(player: GameObject) {
    this.path.map(node => {
      if (this.checkCollision(player, node)) {
        node.checkpoint = true;
      }
    });
  }

  isOnStart(player: GameObject) {
    return this.checkCollision(player, this.startNode);
  }

  isTrackFinished() {
    return this.path.filter(node => node.checkpoint).length == this.path.length;
  }

  checkCollision(player: GameObject, trackNode: TrackObject) {
    return Phaser.Rectangle.intersects(<Phaser.Rectangle>player.sprite.getBounds(), <Phaser.Rectangle>trackNode.sprite.getBounds())
  }
}