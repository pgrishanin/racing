const path = require('path');
var webpack = require('webpack')

// Phaser webpack config
var phaserModule = path.join(__dirname, '/node_modules/phaser-ce/')
var phaser = path.join(phaserModule, 'build/custom/phaser-split.js')
var pixi = path.join(phaserModule, 'build/custom/pixi.js')
var p2 = path.join(phaserModule, 'build/custom/p2.js')

var definePlugin = new webpack.DefinePlugin({
  __DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'true'))
})

module.exports = {
  mode: 'development',
  entry: {
    main: [
      path.resolve(__dirname, './src/index.ts')
    ],
    vendor: ['pixi', 'p2', 'phaser']
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'public/build')
  },
  module: {
    rules: [
      { test: /\.ts?$/, use: 'ts-loader', exclude: /node_modules/ },
      { test: /pixi\.js/, use: ['expose-loader?PIXI'] },
      { test: /phaser-split\.js$/, use: ['expose-loader?Phaser'] },
      { test: /p2\.js/, use: ['expose-loader?p2'] }
    ]
  },
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  },
  plugins: [
    definePlugin
  ],
  optimization: {
    splitChunks: {
      chunks: (chunk) => {
        return chunk.name === 'vendor';
      }
    }
  },
  devtool: 'cheap-source-map',
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    publicPath: '/build/',
    watchContentBase: true,
    port: 9000,
    open: true
  },
  watch: true,
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      'phaser': phaser,
      'pixi': pixi,
      'p2': p2
    }
  }
};